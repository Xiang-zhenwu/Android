// pages/addhomework/addhomework.js
const app = getApp()
const db = wx.cloud.database()
const util = require("../../utils/util.js")

Page({
  data: {
    openid: "",
    showdialog: false,
    choicedialog: false,
    blankdialog: false,
    subjectdialog: false,
    isadding: true,
    type: "",
    titledata: "",
    answerdata: "",
    checkquestion: "",
    homeworktitle: "",
    alphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
    homework: [],
    question: {
      type: "",
      title: "",
      answer: "",
      options: [],
      multiple: false
    },
    optionsinit: [
      {
        option: "A",
        describe: ""
      },
      {
        option: "B",
        describe: ""
      }
    ],
    options: [
      {
        option: "A",
        describe: ""
      },
      {
        option: "B",
        describe: ""
      }
    ]
  },
  onLoad(){
  },
  Cancel(){
    this.setData({
      showdialog: false,
      choicedialog: false,
      blankdialog: false,
      subjectdialog: false,
      isadding: true,
      type: "",
      titledata: "",
      answerdata: "",
      question: {}
    })
  },
  Complete(){
    if(this.data.titledata.length==0&&this.data.isadding){
      wx.showToast({
        title: '请填写题目描述',
        icon: "none"
      })
      return
    }
    if(this.data.type=="choice"&&this.data.answerdata==0&&this.data.isadding){
      wx.showToast({
        title: '请填写选择题答案',
        icon: "none"
      })
      return
    }
    if(this.data.isadding){
      var typequestion = this.data.question
      if(this.data.type=="choice"){
        typequestion.options = this.data.options
        if(this.data.answerdata.length>1){
          typequestion.multiple=true
        }
        else{
          typequestion.multiple=false
        }
      }
      typequestion.type = this.data.type
      typequestion.title = this.data.titledata
      typequestion.answer = this.data.answerdata
      typequestion.do_answer = ""
      this.data.homework.push(typequestion)
      var list = this.data.homework
      this.setData({
        homework: list
      })
    }
    this.setData({
      showdialog: false,
      choicedialog: false,
      blankdialog: false,
      subjectdialog: false,
      isadding: true,
      type: "",
      titledata: "",
      answerdata: "",
      question: {},
      options: this.data.optionsinit
    })
  },
  OpenDialog(event){
    const type = event.target.dataset.info
    this.setData({
      showdialog: true,
      type: type
    })
    if(type=="choice"){
      this.setData({
        choicedialog: true
      })
    }
    else if(type=="blank"){
      this.setData({
        blankdialog: true
      })
    }
    else{
      this.setData({
        subjectdialog: true
      })
    }
  },
  titleData(event){
    this.setData({
      titledata: event.detail.value
    })
  },
  answerData(event){
    this.setData({
      answerdata: event.detail.value
    })
  },
  Homeworktitle(event){
    this.setData({
      homeworktitle: event.detail.value
    })
  },
  OptionDetailBind(event){
    const describe = event.detail.value
    const index = event.target.dataset.info
    this.data.options[index].describe = describe
    this.setData({
      options: this.data.options
    })
  },
  Detail(event){
    const index = event.target.dataset.info
    var typequestion = this.data.homework[index]
    this.setData({
      type: typequestion.type,
      checkquestion: typequestion,
      isadding: false,
      showdialog: true,
    })
    if(typequestion.type == "choice"){
      this.setData({
        choicedialog: true
      })
    }
  },
  Delete(event){
    const index = event.target.dataset.info
    var list = this.data.homework
    list.splice(index, 1)
    this.setData({
      homework: list
    })
  },
  AllComplete(){
    if(this.data.homeworktitle.length==0){
      wx.showToast({
        title: '请添加作业标题',
        icon: "none"
      })
      return
    }
    if(this.data.homework.length==0){
      wx.showToast({
        title: '题目数量不能为零',
        icon: "none"
      })
      return
    }
    var time = util.formatTime(new Date())
    db.collection('homeworks').add({
      data: {
        ownerid: app.globalData.openid,
        ownerInfo: app.globalData.userInfo,
        homeworktitle: this.data.homeworktitle,
        settime: time
      }
    })
    .then(response => {
      const homeworkid = response._id
      for(var item of this.data.homework){
        db.collection('questions').add({
          data: {
            homeworkid: homeworkid,
            detail: item
          }
        })
      }
    })
    wx.switchTab({
      url: '/pages/question_set/question_set',
    })
  },
  AddOption(){
    const length = this.data.options.length
    if(length==7){
      wx.showToast({
        title: '选项不能大于七个',
        icon: "none"
      })
      return
    }
    this.data.options.push({
      option: this.data.alphabet[length+1],
      describe: ""
    })
    this.setData({
      options: this.data.options
    })
  }
})