// pages/question_do/question_do.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    homeworkList: [],
  },
  onLoad(){
    db.collection('do_person').where({
      responserid: app.globalData.openid
    })
    .get({
      success: (response)=>{
        this.setData({
          homeworkList: response.data
        })
      }
    })
  },
  DoHomework(event){

    const index = event.currentTarget.dataset.index
    try {
      wx.setStorageSync('homeworkid', this.data.homeworkList[index].homework[0]._id)
      
      wx.setStorageSync('homeworktitle', this.data.homeworkList[index].homework[0].homeworktitle)
    } catch (e) { }
    wx.navigateTo({
      url: '/pages/homework_detail/homework_detail?how=alter&personid='+app.globalData.openid,
    })
  },
})