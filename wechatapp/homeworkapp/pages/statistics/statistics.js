// pages/statistics/statistics.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    submitnum: 0,
    homeworktitle: "",
    homeworkid:  "",
    responsers: []
  },
  onShow(){
    try {
      var homeworkid = wx.getStorageSync('homeworkid')
      var title = wx.getStorageSync('homeworktitle')
      this.setData({
        homeworkid: homeworkid,
        homeworktitle: title
      })
      db.collection('do_person').where({
        homeworkid: homeworkid
      })
      .get({
        success: (response)=>{
          this.setData({
            submitnum: response.data.length
          })
          for(let item of response.data){
            db.collection('user').where({
              openid: item.responserid
            })
            .get({
              success: (res)=>{
                this.data.responsers.push(res.data[0])
                this.data.responsers[this.data.responsers.length-1]['time'] = item.time
                this.setData({
                  responsers: this.data.responsers
                })
              }
            })
          }
        }
      })
    } catch (e) {
      console.log("error:",e)
    }
  },
  CheckDoDetail(event){
    let index = event.currentTarget.dataset.info
    wx.navigateTo({
      url: '/pages/homework_detail/homework_detail?how=checking&personid='+this.data.responsers[index].openid,
    })
  },
  Test(){
    console.log(this.data.responsers)
  }
})