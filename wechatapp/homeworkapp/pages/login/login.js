// pages/login2/login2.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    homeworkid: "",
    homeworktitle: ""
  },
  onLoad:function(options){  
    this.setData({
      homeworkid: options.homeworkid,
      homeworktitle: options.homeworktitle
    })
    try {
      wx.setStorageSync('homeworkid', this.data.homeworkid)
      wx.setStorageSync('homeworktitle', this.data.homeworktitle)
    } catch (e) { }
  },
  Login(){
    wx.getUserProfile({
      desc: '授权之后才能继续使用本程序',
      success: (response)=>{
        // console.log("授权成功",response.userInfo)
        app.globalData.userInfo = response.userInfo
        wx.cloud.callFunction({
          name: "getOpenId"
        })
        .then(functionResponse=>{
          // console.log(functionResponse)
          app.globalData.openid = functionResponse.result.OPENID
          db.collection('user').where({
            openid: app.globalData.openid
          }).get({
            success: (res)=>{
              if(res.data.length == 0){
                db.collection('user').add({
                  data: {
                    openid: app.globalData.openid,
                    userInfo: app.globalData.userInfo
                  },
                })
              }
            }
          })
        })
        wx.navigateTo({
          url: '/pages/homework_detail/homework_detail?how=doing',
        })
      },
      fail: (res)=>{
        console.log("授权失败",res)
      }
    })
  },
})