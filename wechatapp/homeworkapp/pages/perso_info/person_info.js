// pages/perso_info/person_info.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    userInfo: {}
  },
  onShow(){
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },
  Logout(){
    wx.exitMiniProgram()
  }
})