// pages/question_set/question_set.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {
    showbutton:false,
    homeworkList: [],
    deleteindex: "",
    sendindex: "",
    showdeleteconfirm: false
  },
  onShow(){
    db.collection('homeworks').where({
      ownerid: app.globalData.openid
    })
    .get({
      success: (response)=>{
        this.setData({
          homeworkList: response.data
        })
      }
    })
  },
  addHomework(){
    wx.navigateTo({
      url: '/pages/addhomework/addhomework',
    })
  },
  CheckHomework(event){
    const index = event.currentTarget.dataset.index
    try {
      wx.setStorageSync('homeworkid', this.data.homeworkList[index]._id)
      wx.setStorageSync('homeworktitle', this.data.homeworkList[index].homeworktitle)
    } catch (e) { }
    wx.navigateTo({
      url: '/pages/statistics/statistics',
    })
  },
  ShowButton(event){
    this.setData({
      showbutton: !this.data.showbutton
    })
  },
  SendHomework(event){
    this.setData({
      sendindex: event.target.dataset.index
    })
  },
  onShareAppMessage() {
    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          title: '自定义转发标题'
        })
      }, 2000)
    })
    return {
      title: this.data.homeworkList[this.data.sendindex].homeworktitle,
      path: '/pages/login/login?homeworkid='+this.data.homeworkList[this.data.sendindex]._id+'&homeworktitle='+this.data.homeworkList[this.data.sendindex].homeworktitle,
      imageUrl: '/image/icon.png',
    }
  },
  ShowDelete(event){
    this.setData({
      showdeleteconfirm: true,
      deleteindex: event.target.dataset.index
    })
  },
  CancelDelete(){
    this.setData({
      showdeleteconfirm: false,
      deleteindex: ""
    })
  },
  DeleteHomework(){
    const index = this.data.deleteindex
    var homework = this.data.homeworkList[index]
    var homeworkid = homework._id
    this.data.homeworkList.splice(index,1)
    this.setData({
      homeworkList: this.data.homeworkList,
      showdeleteconfirm: false,
    })
    db.collection('homeworks').where({
      _id: homeworkid
    })
    .remove({
      success: ()=>{
        wx.showToast({
          title: '删除成功！',
          icon: "success"
        })
      }
    })
    db.collection('questions').where({
      homeworkid: homeworkid
    })
    .remove()
    db.collection('do_question').where({
      homeworkid: homeworkid
    })
    .remove()
    db.collection('do_person').where({
      homeworkid: homeworkid
    })
    .remove()
  }
})