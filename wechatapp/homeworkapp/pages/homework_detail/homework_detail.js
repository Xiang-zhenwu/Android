// pages/homework_detail/homework_detail.js
const app = getApp()
const db = wx.cloud.database()
const util = require("../../utils/util.js")

Page({
  data: {
    homeworktitle: "",
    homeworkid:  "",
    homework: {},
    personid: "",
    opentype: "doing",
    allquestions: [],
    choicequestions: [],
    blankquestions: [],
    subjectquestions: [],
    alphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
  },
  onLoad(option){
    this.setData({
      opentype: option.how
    })
    try {
      var value = wx.getStorageSync('homeworkid')
      var title = wx.getStorageSync('homeworktitle')
      this.setData({
        homeworktitle: title,
        homeworkid: value
      })
      db.collection('homeworks').where({
        _id: value
      })
      .get({
        success: (response)=>{
          this.setData({
            homework: response.data
          })
        }
      })
      var table = ""
      var find = {}
      if (option.how == "doing"){
        table = 'questions',
        find = {
          homeworkid: value
        }
      }
      else if(option.how == "alter"||option.how == "checking"){
        table = 'do_question',
        find = {
          homeworkid: value,
          responserid: option.personid
        }
      }
      if (value) {
        db.collection(table).where(find)
        .get({
          success: (response)=>{
            for(var item of response.data){
              if(item.detail.type=="choice"){
                this.data.choicequestions.push(item)
                this.setData({
                  choicequestions: this.data.choicequestions
                })
              }
              else if(item.detail.type=="blank"){
                this.data.blankquestions.push(item)
                this.setData({
                  blankquestions: this.data.blankquestions
                })
              }
              else{
                this.data.subjectquestions.push(item)
                this.setData({
                  subjectquestions: this.data.subjectquestions
                })
              }
            }
          }
        })
      }
    } catch (e) {
      console.log("err")
    }
  },
  onReady(){
  },
  checkBoxChange(event){
    const index = event.currentTarget.dataset.index
    const option = event.detail.value
    var typequestions = this.data.choicequestions
    typequestions[index].detail.do_answer = option
    this.setData({
      choicequestions: typequestions
    })
  },
  BindBlankAnswer(event){
    const index = event.target.dataset.index
    const answer = event.detail.value
    var typequestions = this.data.blankquestions
    typequestions[index].detail.do_answer = answer
    this.setData({
      blankquestions: typequestions
    })
  },
  BindSubjectAnswer(event){
    const index = event.target.dataset.index
    const answer = event.detail.value
    var typequestions = this.data.subjectquestions
    typequestions[index].detail.do_answer = answer
    this.setData({
      subjectquestions: typequestions
    })
  },
  _checkAnswer(data){
    for(var item of data){
      if(!item.detail.do_answer||item.detail.do_answer==""){
        return false
      }
    }
    return true
  },
  _SendToDatabase(data){
    const openid = app.globalData.openid
    db.collection('do_question').where({
      homeworkid: this.data.homeworkid,
      responserid: openid,
    })
    .get({
      success: (response)=>{
        if(response.data.length>0){
          for(var item of data){
            db.collection('do_question').where({
              responserid: openid,
              questionid: item._id,
            })
            .update({
              data: {
                detail: item.detail
              }
            })
          }
        }
        else{
          for(var item of data){
            db.collection('do_question').add({
              data: {
                responserid: openid,
                questionid: item._id,
                homeworkid: item.homeworkid,
                detail: item.detail
              }
            })
          }
        }
      }
    })
  },
  _AddToPersonHomeworkTable(){
    db.collection('do_person').where({
      homeworkid: this.data.homeworkid,
      responserid: app.globalData.openid,
    })
    .get({
      success: (response)=>{
        var time = util.formatTime(new Date())
        if(response.data.length==0){
          db.collection('do_person').add({
            data: {
              responserid: app.globalData.openid,
              homeworkid: this.data.homeworkid,
              homework: this.data.homework,
              time: time
            }
          })
        }
        else{
          db.collection('do_person').where({
            responserid: app.globalData.openid,
            homeworkid: this.data.homeworkid,
          })
          .update({
            data: {
              time: time
            }
          })
        }
      }
    })
  },
  Submit(){
    if(!this._checkAnswer(this.data.choicequestions)||!this._checkAnswer(this.data.blankquestions)||!this._checkAnswer(this.data.subjectquestions)){
      wx.showToast({
        title: '题目未答完',
        icon: 'none'
      })
      return
    }
    else{
      this._SendToDatabase(this.data.choicequestions)
      this._SendToDatabase(this.data.blankquestions)
      this._SendToDatabase(this.data.subjectquestions)
      this._AddToPersonHomeworkTable()
      wx.showToast({
        title: '提交成功',
        icon: 'success'
      })
      wx.switchTab({
        url: '/pages/question_set/question_set',
      })
    }
  }
})